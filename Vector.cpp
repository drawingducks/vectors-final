#include "Vector.h"
#include <stdexcept>

Vector::Vector(const Value* rawArray, const size_t size, float coef) {
    _data = new Value[size];
    _size = size;
    for(int i = 0; i < size; i++) {
        _data[i] = rawArray[i];
    }
    _multiplicativeCoef = coef;
    _capacity = size;
}

Vector::Vector(const Vector& other) {
    _multiplicativeCoef = other._multiplicativeCoef;
    _capacity = other._size;; 
    _size = other._size;
    if(&other == this){
        return;
    }
    _data = new Value[other._size]; 
    for(int k = 0; k < _size; k++) {
        _data[k] = other._data[k];
    }
}

Vector& Vector::operator=(const Vector& other) {
    if(&other == this) {
        return *this;
    }
    _size = other._size;
    _data = new Value[other._size];
    for (int i = 0; i < other._size; i++) {
        _data[i] = other._data[i];
    }
    _multiplicativeCoef = other._multiplicativeCoef;
    _capacity = other._size;
    return *this;
}

Vector::Vector(Vector&& other) noexcept {
    _size = other._size;
    _capacity = other._capacity; 
    _multiplicativeCoef = other._multiplicativeCoef; 
    _data = other._data;
    other._data = nullptr;
    other._size = 0;
    other._capacity = 0;
    other._multiplicativeCoef = 2.0f;
}

Vector&Vector::operator = (Vector&& other) noexcept {
    if (&other == this) {
        return *this;
    }
    else {
        _size = other._size;
        _capacity = other._capacity;
        _multiplicativeCoef = other._multiplicativeCoef;
        _data = other._data;
        other._data = nullptr;
        other._size = 0;
        other._capacity = 0;
        other._multiplicativeCoef = 2.0f;
        return *this;
    }
} 

Vector::~Vector() {
    delete[] _data;
    _size = 0;
    _capacity = 1;
    _multiplicativeCoef = 2.0f;
} 

void Vector::pushBack(const Value &value) {
    _size = _size + 1;
    if(_capacity <= _size - 1) {
        Value* arr = new Value[_capacity];
        if(_capacity < 1) {
            _capacity = 1;
        }
        _capacity *= _multiplicativeCoef;
        for(size_t k = 0; k < _size - 1; k++) {
            arr[k] = _data[k];
        }  
        _data = new Value[_capacity];
        for(size_t k = 0; k < _size - 1; k++) {
            _data[k] = arr[k];
        }
    }
    _data[_size-1] = value;
}

void Vector::pushFront(const Value& value) {
    _size = _size + 1;
    if(_capacity <= _size - 1) {
        Value* arr = new Value[_capacity];
        if(_capacity < 1) {
            _capacity = 1;
        }
        _capacity *= _multiplicativeCoef;
        for(size_t k = 0; k < _size - 1; k++) {
            arr[k] = _data[k];
        }
        _data = new Value[_capacity];
        for(size_t k = 0; k < _size - 1; k++) {
            _data[k] = arr[k];
        }
    }
    for(size_t k = _size - 1; k > 0; k--) {
        _data[k] = _data[k - 1];
    }
    _data[0] = value;
} 

void Vector::popBack() {
    if(_size <= 0) {
        throw std::out_of_range("Smth wrong");
    }
    else if (_size > 0) {
        _size = _size - 1;
    }
} 

void Vector::popFront() {
    if(_size <= 0) {
        throw std::out_of_range("Smth wrong");
    }
    else if (_size > 0) {
        for(int i = 0; i < _size; i++) {
            _data[i] = _data[i + 1];
        }
        _size = _size - 1;
    }
}

void Vector::erase(size_t pos, size_t count) {
    if (_size < pos) {
        throw std::out_of_range("Smth wrong");
    }
    size_t e_count = std::min(count, _size - pos);
    for (size_t  i = 0; i < _size - e_count;  i++) {
        _data[i + pos] = _data[e_count + i + pos]; 
    }
    _size -= e_count;
}

void Vector::eraseBetween(size_t beginPos, size_t endPos) {
    erase(beginPos, endPos - beginPos + 1);
}

size_t Vector::size() const {
    return _size;
}

size_t Vector::capacity() const {
    return _capacity;
}

double Vector::loadFactor() const {
    return double(_size) / _capacity;
}

Value& Vector::operator[](size_t idx) {
    return _data[idx];
}

const Value& Vector::operator[](size_t idx) const {
    return _data[idx];
}

long long Vector::find(const Value& value) const {
    for(int k = 0; k < _size; k++) {
        if(_data[k] == value) {
            return k;
        }
    }
    return -1;
} 

void Vector::reserve(size_t capacity) {
    if(capacity >= _capacity) {
        _capacity = capacity;
        Value* arr = new Value[_capacity];
        for(size_t k = 0; k < _size; k++) {
            arr[k] = _data[k];
        }
        delete[] _data;
        _data = arr;
    }
} 

void Vector::shrinkToFit() {
    if(_capacity != _size) {
        _capacity = _size;
        Value* arr = new Value[_capacity];
        for (size_t k = 0; k < _size; k++) {
            arr[k] = _data[k];
        }
        delete[] _data;
        _data = arr;
    }
} 

void Vector::easyInsert(size_t pos) {
    if (_size < pos) {
        throw std::out_of_range("Smth wrong");
    }
    if (_capacity <= 0) {
        _data = new Value[size_t(_multicativeCoef)];
        _capacity = size_t(_multicativeCoef);
    }
}

void Vector::memoryAll() {
    Value* tmp = new Value[_capacity];
    for (size_t i = 0; i < _size; i++) {
        tmp[i] = _data[i];
    }
    delete[] _data;
    _data = tmp;
}

void Vector::insert(const Value& value, size_t pos) {
    easyInsert(pos);
    if (_size == _capacity) {
        _capacity =  _capacity * _multiplicativeCoef;
        memoryAll();
    }
    for (size_t i = _size; i > pos; i--) {
        _data[i] = _data[i - 1];
    }
    _data[pos] = value;
    _size++;
} 

void Vector::insert(const Value* values, size_t size, size_t pos) {
    easyInsert(pos);
    if (_size + size > _capacity) {
        while(_size + size > _capacity) {
            _capacity *= _multiplicativeCoef;
        }
        memoryAll();
    }
    for (size_t i = _size - 1; i >= pos; i--) {
        _data[size + i] = _data[i];
    }
    for (size_t i = 0; i < size; i++) {
        _data[i + pos] = values[i];
    }
    _size += size;
} 

void Vector::insert(const Vector& vector, size_t pos) {
    insert(vector._data, vector._size, pos);
}

Value& Iterator::operator*() {
    return *_ptr;
}

const Value& Iterator::operator*() const {
    return *_ptr;
}

Value* Iterator::operator->() {
    return _ptr;
}

const Value* Iterator::operator->() const {
    return _ptr;
}

Iterator Iterator::operator++() {
    ++_ptr;
    return *this;
}

Iterator Iterator::operator++(int) {
    Value* n = _ptr;
    ++_ptr;
    return Iterator(n);
}

bool Iterator::operator==(const Iterator& other) const {
    return _ptr == other._ptr;
}

bool Iterator::operator!=(const Iterator& other) const {
    return !(*this == other);
}

Iterator Vector::begin() {
    return Iterator(&_data[0]);
}

Iterator Vector::end() {
    return Iterator(&_data[_size]);
}